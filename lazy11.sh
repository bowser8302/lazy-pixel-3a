#/bin/bash
#
#
USB=$(lsusb | grep Google)
echo $USB
if [[ -z $USB ]]; then
  echo "Device not found on USB. Are you seeing it as being connected?"
  exit 2;
fi
apt update
apt install wget unzip -y
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip $(ls | grep platform)
wget https://dl.google.com/dl/android/aosp/sargo-rq3a.211001.001-factory-2a1befea.zip
unzip sargo-rq3a.211001.001-factory-2a1befea.zip
cd sargo-rq3a.211001.001/
../platform-tools/fastboot --version
sleep 15
../platform-tools/fastboot flash bootloader bootloader-sargo-b4s4-0.3-7357923.img
../platform-tools/fastboot reboot-bootloader
sleep 15
../platform-tools/fastboot flash radio radio-sargo-g670-00116-210406-b-7261833.img
../platform-tools/fastboot reboot-bootloader
sleep 15
../platform-tools/fastboot -w update image-sargo-rq3a.211001.001.zip --skip-reboot
sleep 15
echo "Device Provisioned. Add the..."
echo "carbonite.nowsecure.io/provision-needed: \"\" " 
echo "...annotation to the SoloDevice entry in Cluser Explorer or just provision the device through the Carbonite UI is it's listed there."
#../platform-tools/fastboot devices
