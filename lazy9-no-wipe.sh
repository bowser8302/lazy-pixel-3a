#/bin/bash
#
#
USB=$(lsusb | grep Google)
echo $USB
if [[ -z $USB ]]; then
  echo "Device not found on USB. Are you seeing it as being connected?"
  exit 2;
fi
apt update
apt install wget unzip -y
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip $(ls | grep platform)
wget https://dl.google.com/dl/android/aosp/sargo-pq3b.190705.003-factory-55d7c2c4.zip
unzip sargo-pq3b.190705.003-factory-55d7c2c4.zip
cd sargo-pq3b.190705.003/
../platform-tools/fastboot --version
sleep 10
../platform-tools/fastboot flash bootloader bootloader-sargo-b4s4-0.1-5613380.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot flash radio radio-sargo-g670-00011-190411-b-5457439.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot update image-sargo-pq3b.190705.003.zip --skip-reboot
sleep 15
echo "Device Provisioned. Add the..."
echo "carbonite.nowsecure.io/provision-needed: \"\" " 
echo "...annotation to the SoloDevice entry in Cluser Explorer or just provision the device through the Carbonite UI is it's listed there."
#../platform-tools/fastboot devices
